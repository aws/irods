#!/bin/bash

. ~/irodssettings.sh

cd ~
git clone -b config/cloud-testzone https://gitlab.tudelft.nl/innovation/irods-mango.git
wget https://www.python.org/ftp/python/3.11.7/Python-3.11.7.tgz
tar xzf Python-3.11.7.tgz

# apt-get install -y libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev

apt-get install -y build-essential checkinstall gdb lcov pkg-config \
    libbz2-dev libffi-dev libgdbm-dev libgdbm-compat-dev liblzma-dev \
    libncurses5-dev libreadline6-dev libsqlite3-dev libssl-dev \
    lzma lzma-dev tk-dev uuid-dev zlib1g-dev

apt-get install -y nginx

cd Python-3.11.7
./configure --enable-optimizations
make -s -j$(nproc)
make altinstall

rm /etc/nginx/sites-enabled/default
cat <<EOD > /etc/nginx/sites-enabled/default
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /var/www/html;
        index index.html index.htm index.nginx-debian.html;
        server_name $MANGONAME;
        client_max_body_size 10g;
        location / {
                proxy_pass http://localhost:3000;
                proxy_set_header X-Real-IP \$remote_addr;
        }
}
EOD

certbot --nginx -d $MANGONAME --redirect --non-interactive --agree-tos --no-eff-email --email $LETSENCRYPTSUBSCRIBER

cd ~/irods-mango
/usr/local/bin/python3.11 -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt

cat <<EOD > src/irods_zones_config.py
import os
import ssl
import requests

API_URL = os.environ.get(
    "API_URL", "https://icts-p-coz-data-platform-api.cloud.icts.kuleuven.be"
)
API_TOKEN = os.environ.get("API_TOKEN", "")

DEFAULT_IRODS_PARAMETERS = {
    "port": 1247,
}

ssl_context = ssl.create_default_context(
    purpose=ssl.Purpose.SERVER_AUTH, cafile=None, capath=None, cadata=None
)

DEFAULT_SSL_PARAMETERS = {}

# Dict of irods zones
irods_zones = {
    "$ZONENAME": {
        "jobid": "",
        "parameters": {
            "host": "$PRODUCERNAME",
            "zone": "$ZONENAME",
        },
        "ssl_settings": {'client_server_negotiation': 'request_server_negotiation',
                'client_server_policy': 'CS_NEG_REQUIRE',
                'encryption_algorithm': 'AES-256-CBC',
                'encryption_key_size': 32,
                'encryption_num_hash_rounds': 16,
                'encryption_salt_size': 8,
        },
        "admin_users": ["rods"],
        "logo": "vsc-icon.png",  # path in static folder
        "splash_image": "tu-delft.jpg",
    },
}
EOD

cat <<EOD > src/template_override_rules_tud.yml
---
zone: $ZONENAME

---
zone: all

collection_view_trash:
  source: common/collection_view.html.j2
  target: common/collection_view_trash.html.j2
  matches:
    all:
      subtree: '/{{zone}}/trash' #quotes needed as in principle {} are special characters
object_view_trash:
  source: common/object_view.html.j2
  target: common/object_view_trash.html.j2
  matches:
    all:
      subtree: '/{{zone}}/trash'
EOD

cat <<EOD > /etc/systemd/system/mango.service
[Unit]
Description=Mango server

[Service]
ExecStart=/root/irods-mango/src/run_waitress_tud.sh

[Install]
WantedBy=multi-user.target
EOD

systemctl daemon-reload
systemctl enable mango
systemctl start mango