#!/bin/bash

. ~/irodssettings.sh

psql -h $DB_HOST -U $DB_USER -c "CREATE USER irods WITH PASSWORD '$DB_PASS';"
psql -h $DB_HOST -U $DB_USER -c 'CREATE DATABASE "ICAT";'
psql -h $DB_HOST -U $DB_USER -c 'GRANT ALL PRIVILEGES ON DATABASE "ICAT" TO irods;'