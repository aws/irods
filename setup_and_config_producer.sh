#!/bin/bash

. ../irodssettings.sh

python /var/lib/irods/scripts/setup_irods.py --json_configuration_file=../irods.json -v


# Create certificates from letsencrypt

if [ "$IRODSSSL" = true ]; then

    if [ "$LETSENCRYPT" = true ]; then
        cd ~
        mkdir certstuff
        cd certstuff
        openssl genrsa -out server.key
        openssl req -new -key server.key -out server.csr -subj "/C=NL/ST=Zuid-Holland/L=Delft/O=TU Delft/CN=$PRODUCERNAME"
        certbot certonly --non-interactive --agree-tos --no-eff-email --email $LETSENCRYPTSUBSCRIBER --csr server.csr --standalone
        cat 0000_cert.pem *_chain.pem > chain.pem
        openssl dhparam -2 -out dhparams.pem 2048
        mkdir /etc/irods/letsencrypt
        cp chain.pem dhparams.pem server.key /etc/irods/letsencrypt
        chown -R irods.irods /etc/irods/letsencrypt
    fi

    # Setup irods to require SSL
    cp /etc/irods/core.re /etc/irods/core.re.org
    sed -i /etc/irods/core.re -e 's#^acPreConnect(\*OUT) { \*OUT="CS_NEG_REFUSE"; }#\#acPreConnect(\*OUT) { \*OUT="CS_NEG_REFUSE"; }\nacPreConnect(\*OUT) { \*OUT="CS_NEG_REQUIRE"; }#'
fi

# https://docs.irods.org/4.3.1/system_overview/troubleshooting/#large-number-of-postgresql-odbc-log-files-appearing-in-tmp
sed -i -e 's#^CommLog=1#CommLog=0#' /etc/odbcinst.ini

# Properly start the irods server
su -l irods -c "./irodsctl start"

mkdir /data
chmod 755 /data
chown -R irods.irods /data

su - irods -c "iadmin mkresc main passthru"
su - irods -c "iadmin mkresc irods_data unixfilesystem ${PRODUCERNAME}:/data/data1"
su - irods -c "iadmin addchildtoresc main irods_data"

sed -i /etc/irods/core.re -e \
    's#acSetRescSchemeForCreate {msiSetDefaultResc("initialResc","null"); }#acSetRescSchemeForCreate {msiSetDefaultResc("main","null"); }#'
sed -i /etc/irods/core.re -e \
    's#acSetRescSchemeForRepl {msiSetDefaultResc("initialResc","null"); }#acSetRescSchemeForCreate {msiSetDefaultResc("main","null"); }#'

su - irods -c "iadmin rmresc initialResc"
