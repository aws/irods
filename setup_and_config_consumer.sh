#!/bin/bash

. ../irodssettings.sh

PYTHONPATH=/var/lib/irods/scripts ./custom_setup_irods_consumer.py --json_configuration_file=../irods.json

# Create certificates from letsencrypt

if [ "$LETSENCRYPT" = true ]; then
    cd ~
    mkdir certstuff
    cd certstuff
    openssl genrsa -out server.key
    openssl req -new -key server.key -out server.csr -subj "/C=NL/ST=Zuid-Holland/L=Delft/O=TU Delft/CN=$CONSUMERNAME"
    certbot certonly --non-interactive --agree-tos --no-eff-email --email $LETSENCRYPTSUBSCRIBER --csr server.csr --standalone
    cat 0000_cert.pem *_chain.pem > chain.pem
fi

if [ "$IRODSSSL" = true ]; then
    cd ~/certstuff
    openssl dhparam -2 -out dhparams.pem 2048
    mkdir /etc/irods/letsencrypt
    cp chain.pem dhparams.pem server.key /etc/irods/letsencrypt
    chown -R irods.irods /etc/irods/letsencrypt


    # Setup irods to require SSL
    cp /etc/irods/core.re /etc/irods/core.re.org
    sed -i /etc/irods/core.re -e 's#^acPreConnect(\*OUT) { \*OUT="CS_NEG_REFUSE"; }#\#acPreConnect(\*OUT) { \*OUT="CS_NEG_REFUSE"; }\nacPreConnect(\*OUT) { \*OUT="CS_NEG_REQUIRE"; }#'
fi

# Properly start the irods server
su -l irods -c "./irodsctl start"

mkdir /data
chmod 755 /data
chown -R irods.irods /data

su - irods -c "iadmin mkresc second unixfilesystem ${CONSUMERNAME}:/data/data1"
