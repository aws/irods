#!/bin/bash

wget -qO - https://packages.irods.org/irods-signing-key.asc | apt-key add -
echo "deb [arch=amd64] https://packages.irods.org/apt/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/renci-irods.list
echo START: apt-get update
apt-get update
echo START: apt-get upgrade -y
apt-get upgrade -y
echo START: apt-get install -y irods-server irods-database-plugin-postgres awscli
apt-get install -y irods-server irods-database-plugin-postgres awscli jq

echo START: Creating values from secret
DB_SECRETS_JSON=$(aws secretsmanager get-secret-value --secret-id $POSTGRESSECRETID$ --region $AWS_REGION$ | jq -r '.SecretString')

DB_USER=$(echo $DB_SECRETS_JSON | jq -r '.username')
DB_PASS=$(echo $DB_SECRETS_JSON | jq -r '.password')
DB_HOST=$(echo $DB_SECRETS_JSON | jq -r '.host')
DB_PORT=$(echo $DB_SECRETS_JSON | jq -r '.port')
DB_INSTANCE=$(echo $DB_SECRETS_JSON | jq -r '.dbInstanceIdentifier')

CONTROLPLANEKEY=$(aws secretsmanager get-secret-value --secret-id $CONTROLPLANEARN$ --region $AWS_REGION$ | jq -r '.SecretString')
ZONEKEY=$(aws secretsmanager get-secret-value --secret-id $ZONEARN$ --region $AWS_REGION$ | jq -r '.SecretString')
NEGOTIATIONKEY=$(aws secretsmanager get-secret-value --secret-id $NEGOTIATIONARN$ --region $AWS_REGION$ | jq -r '.SecretString')
ADMINPASSWORD=$(aws secretsmanager get-secret-value --secret-id $ADMINPASSWORDARN$ --region $AWS_REGION$ | jq -r '.SecretString')

echo START: writing to ~/dbsettings.sh
cat <<EOD > ~/dbsettings.sh
export DB_USER=$DB_USER
export DB_PASS=$DB_PASS
export DB_HOST=$DB_HOST
export DB_PORT=$DB_PORT
export DB_INSTANCE=$DB_INSTANCE
export PGPASSWORD=$DB_PASS

export CONTROLPLANEKEY=$CONTROLPLANEKEY
export ZONEKEY=$ZONEKEY
export NEGOTIATIONKEY=$NEGOTIATIONKEY
export ADMINPASSWORD=$ADMINPASSWORD
EOD

echo START: writing to ~/createdb.sh
cat  <<EOD > ~/createdb.sh
export DB_USER=$DB_USER
export DB_PASS=$DB_PASS
export DB_HOST=$DB_HOST
export DB_PORT=$DB_PORT
export DB_INSTANCE=$DB_INSTANCE
export PGPASSWORD=$DB_PASS

export CONTROLPLANEKEY=$CONTROLPLANEKEY
export ZONEKEY=$ZONEKEY
export NEGOTIATIONKEY=$NEGOTIATIONKEY
export ADMINPASSWORD=$ADMINPASSWORD

psql -h $DB_HOST -U $DB_USER -c "CREATE USER irods WITH PASSWORD '$DB_PASS';"
psql -h $DB_HOST -U $DB_USER -c 'CREATE DATABASE "ICAT";'
psql -h $DB_HOST -U $DB_USER -c 'GRANT ALL PRIVILEGES ON DATABASE "ICAT" TO irods;'
EOD

echo START: writing to ~/setupirods.sh
cat <<EOD > ~/setupirods.sh

python /var/lib/irods/scripts/setup_irods.py < ~/irods.input

echo 'Now run: sudo su irods -c "/var/lib/irods/irodsctl start"'

EOD

echo START: writing to ~/irods.input
cat <<EOD > ~/irods.input
irods
irods
1
1
$DB_HOST
$DB_PORT
ICAT
irods
y
$DB_PASS
IRODSALT
tempZone
1247
20000
20199
1248
file:///var/lib/irods/configuration_schemas
rods
y
$ZONEKEY
$NEGOTIATIONKEY
$CONTROLPLANEKEY
$ADMINPASSWORD
/var/lib/irods/Vault

EOD

echo START: writing to /tmp/debugdata
cat <<'EOD' >/tmp/debugdata

$CREATIONDATA$

EOD

echo END: done running userdata script
