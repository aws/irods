#!/bin/bash

. ~/irodssettings.sh

DEFAULTRESC=initialResc
UPDATEDRESC=main

#IRODSPROVIDERHOST=$(curl http://169.254.169.254/latest/meta-data/public-hostname)
#IRODSPROVIDERHOST=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
#IRODSPROVIDERHOST=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)
IRODSPROVIDERHOST=$(curl http://169.254.169.254/latest/meta-data/local-hostname)

LOCALIP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)
LOCALNAME=$(curl http://169.254.169.254/latest/meta-data/local-hostname)
HOSTNAME=$(hostname)


cat <<EOD > ~/irods.json
{
    "admin_password": "$ADMINPASSWORD",
    "default_resource_directory": "/var/lib/irods/Vault",
    "default_resource_name": "$DEFAULTRESC",
    "host_system_information": {
        "service_account_user_name": "irods",
        "service_account_group_name": "irods"
    },
    "service_account_environment": {
        "irods_client_server_negotiation": "request_server_negotiation",
        "irods_client_server_policy": "CS_NEG_DONT_CARE",
        "irods_connection_pool_refresh_time_in_seconds": 300,
        "irods_cwd": "/$ZONENAME/home/rods",
        "irods_default_hash_scheme": "SHA256",
        "irods_default_number_of_transfer_threads": 4,
        "irods_default_resource": "$UPDATEDRESC",
        "irods_encryption_algorithm": "AES-256-CBC",
        "irods_encryption_key_size": 32,
        "irods_encryption_num_hash_rounds": 16,
        "irods_encryption_salt_size": 8,
        "irods_home": "/$ZONENAME/home/rods",
        "irods_host": "$PRODUCERNAME",
        "irods_match_hash_policy": "compatible",
        "irods_maximum_size_for_single_buffer_in_megabytes": 32,
        "irods_port": 1247,
        "irods_server_control_plane_encryption_algorithm": "AES-256-CBC",
        "irods_server_control_plane_encryption_num_hash_rounds": 16,
        "irods_server_control_plane_key": "$CONTROLPLANEKEY",
        "irods_server_control_plane_port": 1248,
        "irods_transfer_buffer_size_for_parallel_transfer_in_megabytes": 4,
        "irods_user_name": "rods",
        "irods_zone_name": "$ZONENAME",
        "schema_name": "service_account_environment",
        "schema_version": "v4",
        "irods_ssl_certificate_chain_file": "/etc/irods/letsencrypt/chain.pem",
        "irods_ssl_certificate_key_file": "/etc/irods/letsencrypt/server.key",
        "irods_ssl_dh_params_file": "/etc/irods/letsencrypt/dhparams.pem"
    },
    "server_config": {
        "advanced_settings": {
            "default_log_rotation_in_days": 5,
            "default_number_of_transfer_threads": 4,
            "default_temporary_password_lifetime_in_seconds": 120,
            "delay_rule_executors": [
                "irods-provider"
            ],
            "delay_server_sleep_time_in_seconds": 30,
            "dns_cache": {
                "eviction_age_in_seconds": 3600,
                "shared_memory_size_in_bytes": 5000000
            },
            "hostname_cache": {
                "eviction_age_in_seconds": 3600,
                "shared_memory_size_in_bytes": 2500000
            },
            "maximum_size_for_single_buffer_in_megabytes": 32,
            "maximum_size_of_delay_queue_in_bytes": 0,
            "maximum_temporary_password_lifetime_in_seconds": 1000,
            "number_of_concurrent_delay_rule_executors": 4,
            "stacktrace_file_processor_sleep_time_in_seconds": 10,
            "transfer_buffer_size_for_parallel_transfer_in_megabytes": 4,
            "transfer_chunk_size_for_parallel_transfer_in_megabytes": 40
        },
        "catalog_provider_hosts": [
            "$PRODUCERNAME"
        ],
        "catalog_service_role": "provider",
        "client_api_allowlist_policy": "enforce",
        "controlled_user_connection_list": {
            "control_type": "denylist",
            "users": []
        },
        "default_dir_mode": "0750",
        "default_file_mode": "0600",
        "default_hash_scheme": "SHA256",
        "default_resource_name": "$DEFAULTRESC",
        "environment_variables": {
            "IRODS_DATABASE_USER_PASSWORD_SALT": "IRODSALT"
        },
        "federation": [],
        "host_access_control": {
            "access_entries": []
        },
        "host_resolution": {
            "host_entries": [
                {
                    "address_type": "local",
                    "addresses": [
                        "$PRODUCERNAME",
                        "$HOSTNAME",
                        "$LOCALNAME",
                        "$LOCALIP"
                    ]
                }
            ]
        },
        "log_level": {
            "agent": "info",
            "agent_factory": "info",
            "api": "info",
            "authentication": "info",
            "database": "info",
            "delay_server": "info",
            "legacy": "info",
            "microservice": "info",
            "network": "info",
            "resource": "info",
            "rule_engine": "info",
            "server": "info",
            "sql": "info"
        },
        "match_hash_policy": "compatible",
        "negotiation_key": "$NEGOTIATIONKEY",
        "plugin_configuration": {
            "authentication": {},
            "database": {
                "postgres": {
                    "db_host": "$DB_HOST",
                    "db_name": "ICAT",
                    "db_odbc_driver": "PostgreSQL Unicode",
                    "db_password": "$DB_PASS",
                    "db_port": $DB_PORT,
                    "db_username": "irods"
                }
            },
            "network": {},
            "resource": {},
            "rule_engines": [
            {
                "instance_name": "irods_rule_engine_plugin-irods_rule_language-instance",
                "plugin_name": "irods_rule_engine_plugin-irods_rule_language",
                "plugin_specific_configuration": {
                    "re_data_variable_mapping_set": [
                        "core"
                    ],
                    "re_function_name_mapping_set": [
                        "core"
                    ],
                    "re_rulebase_set": [
                        "core"
                    ],
                    "regexes_for_supported_peps": [
                        "ac[^ ]*",
                    "msi[^ ]*",
                    "[^ ]*pep_[^ ]*_(pre|post|except|finally)"
                    ]
                },
                "shared_memory_instance": "irods_rule_language_rule_engine"
            },
            {
                "instance_name": "irods_rule_engine_plugin-cpp_default_policy-instance",
                "plugin_name": "irods_rule_engine_plugin-cpp_default_policy",
                "plugin_specific_configuration": {}
            }
            ]
        },
        "rule_engine_namespaces": [
            ""
        ],
        "schema_name": "server_config",
        "schema_validation_base_uri": "file:///var/lib/irods/configuration_schemas",
        "schema_version": "v4",
        "server_control_plane_encryption_algorithm": "AES-256-CBC",
        "server_control_plane_encryption_num_hash_rounds": 16,
        "server_control_plane_key": "$CONTROLPLANEKEY",
        "server_control_plane_port": 1248,
        "server_control_plane_timeout_milliseconds": 10000,
        "server_port_range_end": 20199,
        "server_port_range_start": 20000,
        "xmsg_port": 1279,
        "zone_auth_scheme": "native",
        "zone_key": "$ZONEKEY",
        "zone_name": "$ZONENAME",
        "zone_port": 1247,
        "zone_user": "rods"
    }
}
EOD
