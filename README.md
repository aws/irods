
## Applying a stack:

`aws cloudformation deploy --template-file irods.yaml --capabilities CAPABILITY_IAM --stack-name new-irods`

## Applying a stack with parameters:

`aws cloudformation deploy --template-file irods.yaml --capabilities CAPABILITY_IAM --stack-name new-irods --parameter-overrides MakeTestServer=false`

## Applying a stack with parameters from a file:

`aws cloudformation deploy --template-file irods.yaml --capabilities CAPABILITY_IAM --stack-name new-irods --parameter-overrides file://producer+consumer.json`

## Deleting a stack

`aws cloudformation delete-stack --stack-name new-irods`


## Installing irods on producer

```
% cd irods
% ./createdb.sh
% ./createproducerirodsinput.sh
% python /var/lib/irods/scripts/setup_irods.py --json_configuration_file ~/irods.json
```

## Certificates + SSL

```
% snap install --classic certbot
% openssl genrsa -out server.key
% openssl req -new -key server.key -out server.csr -subj "/C=NL/ST=Zuid-Holland/L=Delft/O=TU Delft/CN=irods.cloud.tudelft.ninja"
% certbot certonly --csr server.csr
or rather:
% certbot certonly --non-interactive --agree-tos --no-eff-email --email m.m.a.schenk@tudelft.nl --csr server.csr --standalone
% cat 0000_cert.pem *_chain.pem > chain.pem
% openssl dhparam -2 -out dhparams.pem 2048
% mkdir /etc/irods/letsencrypt
% cp chain.pem dhparams.pem server.key /etc/irods/letsencrypt
```

Then add

```json
"irods_ssl_certificate_chain_file": "/etc/irods/letsencrypt/chain.pem",
"irods_ssl_certificate_key_file": "/etc/irods/letsencrypt/server.key",
"irods_ssl_dh_params_file": "/etc/irods/letsencrypt/dhparams.pem"
```
to ~irods/.irods/irods_environment.json

and change the file /etc/irods/core.re replacing the line reading

```c
acPreConnect(*OUT) { *OUT="CS_NEG_REFUSE"; }
```

with
```c
acPreConnect(*OUT) { *OUT="CS_NEG_REQUIRE"; }
```

```mermaid

graph TB

irods3:replication --> irods2_data2:unixfilesystem
irods3:replication --> irods_data2:unixfilesystem

subgraph irods2

direction TB

irods2_data2:unixfilesystem
irods2_data1:unixfilesystem

end

subgraph irods

direction TB

irods_data1:unixfilesystem
irods_data2:unixfilesystem

end

irods1:replication --> irods_data1:unixfilesystem
irods2:replication --> irods2_data1:unixfilesystem



```